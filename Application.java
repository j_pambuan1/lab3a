public class Application{
	public static void main(String[] args){
		Student student = new Student();
		student.height = 175;
		student.program = "Computer Science";
		
		Student studentTwo = new Student();
		studentTwo.height = 190;
		studentTwo.program = "Engineering";
		
		//System.out.println(student.height);
		//System.out.println(student.program);
		
		//student.sayHeight();
		//student.sayProgram();
		
		//System.out.println("~~~");
		//studentTwo.sayHeight();
		//studentTwo.sayProgram();

		Student[] section4 = new Student[3];
		section4[0] = student;
		section4[1] = studentTwo;
		//System.out.println(section4[2].height);
		
		section4[2] = new Student();
		section4[2].height = 190;
		section4[2].age = 23;
		section4[2].program = "Computer Science";
		System.out.println(section4[2].height);
		System.out.println(section4[2].age);
		System.out.println(section4[2].program);
	}
}